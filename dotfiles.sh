#!/bin/sh
# install my dotfiles on any arch linux based machine.
# NAME contains the user to install dotfiles to
# TODO: Add color to misc options in /etc/pacman.conf
set -eu pipefail

cd "/home/$NAME"

user() {
	sudo -u "$NAME" "$@"
}

have() {
	command -v "$@" >/dev/null 2>&1
}

install() {
	pacman -S --noconfirm --needed "$@"
}

# Install needed programs
install  \
	sudo git make \
	neovim \
	python python-pip python-neovim \
	zsh wget curl pv \
	xorg xorg-xinit xclip dmenu sxhkd unclutter xdotool \
	ttf-dejavu powerline-fonts \
	feh dunst libnotify highlight \
	chromium surf \
	nmap mpv \
	pulseaudio pulsemixer \
	openssh zathura zathura-pdf-mupdf rsync \
	tor docker \
	i3lock xssstate scrot imagemagick maim rlwrap \
	base-devel \
	xf86-video-intel

# Download the git repo and place it in the users home directory
dotrepo="https://gitlab.com/0u/dotfiles.git"
dest="/home/$NAME"

dir=$(mktemp -d)
[ ! -d "$dest" ] && mkdir -p "$2" && chown -R "$NAME:wheel" "$2"
chown -R "$NAME:wheel" "$dir"
user git clone --recursive --depth 1 "$dotrepo" "$dir/gitrepo"
user cp -rfT "$dir/gitrepo" "$dest"

# Build dwm
user make -C dwm

# Install vim plugins
user nvim -Ec 'PlugInstall|UpdateRemotePlugins|qa'

# Install st
user git clone https://github.com/lukesmithxyz/st.git
make -C st install

# Enable pulseaudio
user systemctl --user enable pulseaudio.socket


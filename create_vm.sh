#!/bin/sh
# This script sets up a virtualbox VM for arch linux testing.
# It downloads the ISO if needed and sets all virtualbox settings.
set -eu pipefail

# 1. Get the arch linux ISO
# 	ask the user for location, if they do not specify then download
# 2. Create virtualbox VM with correct settings
#		(might need to tweak networking settings globally).

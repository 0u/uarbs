#!/bin/bash
# This file spins up a VM to test the installation script.
# Requires virtualbox and VBoxManage.
# Also requires a VM called 'uarbs' that is at least
# 8GB Disk
# 1GB Memory
# has a live archiso image attached.
# is on host-only networking
# and that the host has ~/.ssh/id_rsa.pub
set -eu pipefail

BOOTMENU=3  # time to wait till boot menu appears

# time to wait till machine goes from having network to fully booted with a shell
BOOTTIME=15

# VM's IP on the host only network.
IP=192.168.56.101

ssh_options="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

status="$(vboxmanage showvminfo uarbs | grep State | awk '{print $2}')"
if [ "$status" != "running" ]; then
	VBoxManage startvm uarbs --type headless

	# wait for boot menu
	echo "Waiting $BOOTMENU seconds for the boot menu"
	sleep $BOOTMENU

	# Push return on the grub boot prompt
	VBoxManage controlvm uarbs keyboardputstring $'\n'

	# Wait for arch to boot
	echo "Waiting for arch to boot"
	while ! ping $IP -c 1 2>/dev/null 1>&2; do sleep 3; done
	echo "Sleeping for $BOOTTIME seconds"
	sleep $BOOTTIME

	echo "Typing keyboard.sh into VM"

	# Switch to bash, was having a wierd issue with zsh.
	VBoxManage controlvm uarbs keyboardputstring $'clear; exec bash\n'

	# Start sshd, add our pub key to ~/.ssh/authorized_keys etc
	VBoxManage controlvm uarbs keyboardputfile keyboard.sh

	# Send our public key over to use for credentials
	echo "Sending our public key to the VM"
	ncat $IP 1337 -w 1 < ~/.ssh/id_rsa.pub
fi

# copy files over
uarbs="$(dirname "$(realpath "$0")")"
ssh $ssh_options root@$IP rm -rf ~/uarbs
scp $ssh_options -rq "$uarbs" root@$IP:~/uarbs

# ssh into the machine
ssh $ssh_options root@$IP

#!/bin/sh
# Everything needed to install my arch setup on a new computer
# TODO:
#	Figure out if i can stuff it all into one monster script or if i need more then one.

set -eu pipefail

###############################################################################
#                        Configuration variables.                             #
###############################################################################

# Timezone for the installation
TIMEZONE=America/New_York

# Locale to uncomment in /etc/locale.gen and to put in /etc/locale.conf
LOCALE=en_US.UTF-8

# Hostname for the installation
HOSTNAME=arch

# EFI system partition mount point
ESP=/efi

# User to create and install my configuration for.
NAME=peep

###############################################################################
#                          all the subroutines                                #
###############################################################################

# extra stuff that lets you just type y without pressing enter.
# old_stty_cfg=$(stty -g)
# stty raw -echo
# stty "$old_stty_cfg"
promptYN() {
	printf "%s (y/n) " "$*"
	read -r yesorno
	[ "$yesorno" = "y" ]
}

# output $* to stderr then exits with an exit code of 1
abort() {
	printf "\e[0;31m[E] %s\n" "$*" 1>&2
	exit 1
}

# get a file from this repo
get() {
	curl -LO https://gitlab.com/0u/uarbs/raw/master/"$*"
}

# verify that the system can have arch installed on it.
checks() {
	# Make sure we are root.
	if [ "$(id -u)" != "0" ]; then
	   abort "This script must be run as root"
	fi

	# Find out if we are booting in UEFI mode.
	if [ -d /sys/firmware/efi/efivars ]; then
		echo "UEFI Installation selected"
		UEFI=1
	else
		UEFI=0
		echo "BIOS Installation selected"
	fi

	# Make sure we have a connection to the internet.
	if ! ping -c 1 8.8.8.8 >/dev/null 2>&1; then
		abort "You do not have a connection to the internet."
	fi

	# Prompt for confirmation
	echo "This will install arch linux on your computer OVERWRITING EVERYTHING ON IT"
	if ! promptYN "Are you sure you wish to proceed?"; then
		abort "why did you run this then ._."
	fi
}

# initialize the system before installation.
init() {
	# Use timedatectl(1) to ensure the system clock is accurate
	timedatectl set-ntp true

	# syncronize package databases
	pacman -Sy

	# Install a larger font for the TTY if they want to.
	if promptYN "Install a larger font for the installation?"; then
		pacman -S --noconfirm terminus-font
		setfont ter-v32n
		# export needed because this will be checked inside chroot.
		export NEWFONT="ter-v32n"
	fi

	# Unmount /mnt if needed TODO Test
	if mount | grep ' /mnt '; then
		umount -R /mnt
	fi
}

# prompt the user for the disk to install arch unto, then
# store it in the global variable disk.
getdisk() {
	sfdisk -l
	printf "Please select a disk to install arch into: "
	read -r disk

	if ! [ -b "$disk" ]; then
		abort "disk '$disk' does not exist or is not a block device."
	fi

	if ! promptYN "Are you sure '$disk' is correct?"; then
		abort "scaredy cat"
	fi
}

# Partition and format for a uefi scheme.
partition_uefi() {
	sfdisk "$disk" <<-EOF
	# first set the partition table to be GPT
	label: gpt
	# then create the EFI system partition
	,512M,U
	# then use the rest for linux.
	,
EOF

	echo "Formatting the partitions..."
	mkfs.ext4 "${disk}2"
	mkfs.fat -F32 "${disk}1"
}

# Partition and format for a bios scheme.
# TODO
partition_bios() {
	sfdisk "$disk" <<-EOF
	EOF
	echo "Formatting the partitions..."
}

# partition the disk $disk.
# also format the new partitions
partition() {
	echo "Partitioning the disk '$disk'"
	if [ $UEFI = 1 ];
		then partition_uefi
		else partition_bios
	fi
}

# install the system to $disk2
install() {
	mount "${disk}2" /mnt
	trap 'umount -R /mnt' INT
	mkdir -p "/mnt/$ESP"

	mount "${disk}1" "/mnt/$ESP"

	echo "Installing the system! this may take a while..."
	pacstrap /mnt base base-devel

	echo "Generating fstab"
	genfstab -U /mnt >> /mnt/etc/fstab
}

adduser() {
	# Adds user `$1` and prompts for a password
	echo "Adding user \"$1\"..."
	useradd -m -g wheel -s /bin/bash "$1" >/dev/null 2>&1 ||
	usermod -a -G wheel "$1" && mkdir -p /home/"$1" && chown "$1":wheel /home/"$1"
	passwd "$1"
}

# installation phase that happens inside the chroot.
chroot_install() {
	echo "Setting your time zone to $TIMEZONE"
	ln -svf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime

	echo "Running hwclock(8) to generate /etc/adjtime"
	hwclock --systohc

	echo "Generating locales"
	# Uncomment the line with our selected locale
	sed -i "/#${LOCALE}/s/^#//g" /etc/locale.gen
	locale-gen

	echo "'LANG=$LOCALE' -> /etc/locale.conf"
	echo "LANG=$LOCALE" > /etc/locale.conf

	echo "$HOSTNAME" > /etc/hostname
	cat > /etc/hosts <<EOF
127.0.0.1	localhost
::1	localhost
127.0.1.1	$HOSTNAME.localdomain	$HOSTNAME
EOF

	echo "Installing the bootloader, this is the most iffy part, if the computer
does not boot please check your BIOS and make sure secureboot is off."
	pacman --noconfirm -S grub efibootmgr
	grub-install --target=x86_64-efi --efi-directory="$ESP" --bootloader-id=GRUB
	grub-mkconfig -o /boot/grub/grub.cfg

	echo "Enabling dhcpcd via systemctl."
	pacman -S --noconfirm --needed dhcpcd
	systemctl enable dhcpcd

	if [ -n "${NEWFONT+x}" ]; then
		echo "Persistently setting the font in /etc/vconsole.conf to $NEWFONT"
		echo "FONT=$NEWFONT" > /etc/vconsole.conf
	fi

	echo "Creating user $NAME"
	adduser "$NAME"

	if promptYN "Install my dotfiles?"; then
		# install my dotfiles by running my seperate dotfiles.sh script.
		echo "Downloading dotfiles.sh script..."
		get dotfiles.sh
		NAME="$NAME" bash dotfiles.sh
		rm dotfiles.sh
	fi

	if promptYN "[EXPERIMENTAL] Create swap file? this is required for hibernation."; then
		printf "Swap size: "
		read -r size
		fallocate -l "$size" /swapfile

		echo '/swapfile none swap sw 0 0' >> /etc/fstab
	fi
}

enter_chroot() {
	echo "Chrooting into the system"
	cp "$(realpath "$0")" /mnt/install.sh
	IN_CHROOT="YES" arch-chroot /mnt bash install.sh
	rm /mnt/install.sh
}

###############################################################################
#                          stuff finally happens                              #
###############################################################################

if [ -z "${IN_CHROOT+x}" ]; then
	checks       # check that the system is suitable for installation
	init         # initialize the installation process
	getdisk      # get the disk to install into
	partition    # partition the disk given
	install      # install the system into the partitions
	enter_chroot # run the chroot installation phase
else
	chroot_install
fi
